import type { ProductType } from './product';

export type CartProductType = {
  product: ProductType;
  qty: number;
};
export type CartType = {
  products: CartProductType[];
  totalPrice: number;
};

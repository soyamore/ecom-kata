import type { ProductType } from '~/types';

const useProduct = () => {
  const products = useState('products', () => [] as ProductType[]);
  const currentProduct = useState('current-product', () => ({} as ProductType));

  const getProducts = async () => {
    const { data, error, refresh } = await useFetch(
      'https://fakestoreapi.com/products'
    );
    products.value = data.value as ProductType[];
  };

  const getProductsByCategory = async (category: string) => {
    const { data, error, refresh } = await useFetch(
      `https://fakestoreapi.com/products/category/${category}`
    );
    products.value = data.value as ProductType[];
  };

  const getProduct = async (id: number) => {
    const { data, error, refresh } = await useFetch(
      `https://fakestoreapi.com/products/${id}`
    );
    currentProduct.value = data.value as ProductType;
  };

  const maxProductPrice = computed(() => {
    return Math.max(...products.value.map((product) => product.price));
  });
  return {
    getProduct,
    getProducts,
    getProductsByCategory,
    maxProductPrice,
    currentProduct,
    products,
  };
};

export default useProduct;

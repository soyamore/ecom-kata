const useMenu = () => {
  const menu = useState('menu', () => [] as string[]);

  const loadMenuLinks = async () => {
    const { data } = await useFetch('https://fakestoreapi.com/products/categories');
    menu.value = data.value as string[];
  };
  return {
    loadMenuLinks,
    menu,
  };
};

export default useMenu;

import type { CartType, ProductType, CartProductType } from '~/types';
const useCart = () => {
  const cart = useState(
    'cart',
    () => ({ products: [], totalPrice: 0 } as CartType)
  );

  const addProductToCart = (product: ProductType): void => {
    let index = cart.value.products?.findIndex(
      (item) => product.id === item.product.id
    );
    if (index !== -1 && index !== undefined) {
      cart.value.products[index].qty += 1;
      cart.value.totalPrice += product.price;
    } else {
      const item: CartProductType = { product, qty: 1 };
      console.log(item);
      cart.value.products.push(item);
      cart.value.totalPrice += product.price;
    }
  };

  const removeProductFromCart = (id: number): void => {
    cart.value.products = cart.value.products.filter(
      (item: CartProductType) => {
        if (item.product.id === id) {
          cart.value.totalPrice -= item.product.price * item.qty;
        } else {
          return item;
        }
      }
    );
  };

  return {
    addProductToCart,
    removeProductFromCart,
    cart,
  };
};

export default useCart;

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: ['@nuxt/ui'],
  vite: {
    server: {
      proxy: {
        '/api/': {
          // using proxy to avoid issues like cors and to avoid exposing the api.
          target: 'https://fakestoreapi.com/',
          changeOrigin: true,
          //rewrite: (path) => path.replace(/^\/api/, ''),
        },
      },
    },
  },
});
